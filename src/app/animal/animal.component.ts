import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.css']
})
export class AnimalComponent implements OnInit {

  animal
  nameAnimal
  selectAnimal
  texto
  texto2
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    
  }

  print(){
    console.log(this.texto,)
    console.log(this.texto2)
  }

  getAnimal () {
    (console.log(this.selectAnimal))

    let url = "http://localhost:8080/animal/"+this.selectAnimal;
     this.http.get(url).subscribe(response =>{ 
       this.animal = response
    });
  }
}
