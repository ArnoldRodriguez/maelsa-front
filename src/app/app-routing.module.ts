

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InfouserComponent } from './infouser/infouser.component';
import { CabelloComponent } from './cabello/cabello.component';
import { MachinesComponent } from './machines/machines.component';
import { Error404Component } from './error404/error404.component';
import {AnimalComponent} from './animal/animal.component'

const routes: Routes = [
  { path: 'infouser1', component: InfouserComponent },
  { path: 'cabello', component: CabelloComponent },
  { path: 'machines', component: MachinesComponent },
  { path: 'animal', component:AnimalComponent},
  { path: '404', component: Error404Component },
  { path: '**', redirectTo: "404" },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }