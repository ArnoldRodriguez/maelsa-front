import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MachinesComponent } from './machines/machines.component';
import { HttpClientModule } from '@angular/common/http';
import { InfouserComponent } from './infouser/infouser.component';
import { Userinfo1Component } from './userinfo1/userinfo1.component';
import { CabelloComponent } from './cabello/cabello.component';
import { AppRoutingModule } from './app-routing.module';
import { Error404Component } from './error404/error404.component';
import { AnimalComponent } from './animal/animal.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MachinesComponent,
    InfouserComponent,
    Userinfo1Component,
    CabelloComponent,
    Error404Component,
    AnimalComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
