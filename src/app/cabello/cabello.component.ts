import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-cabello',
  templateUrl: './cabello.component.html',
  styleUrls: ['./cabello.component.css']
})
export class CabelloComponent implements OnInit {

  cabello
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getCabello();
  }

  getCabello () {
    var url = environment.urlBsckend+"cabello";   
     this.http.get(url).subscribe(datam => {
      console.log(datam)
      this.cabello = datam;
      
    });
    
  }

}
