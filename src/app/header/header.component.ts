import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  itemsnavba
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getNavBarItem();
  }

  getNavBarItem () {
    var url = environment.urlBsckend+"itemsnavba";
     this.http.get(url).subscribe(datam => {
      console.log(datam)
      this.itemsnavba = datam["list"];
      console.log(this.itemsnavba)
    });
  }

}
