import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-infouser',
  templateUrl: './infouser.component.html',
  styleUrls: ['./infouser.component.css']
})
export class InfouserComponent implements OnInit {

  constructor(private http: HttpClient) { }

  userInfo:any
  ngOnInit(): void {
    this.getUser();
    this.getUserLog();
    this.getUserDel();
    this.getUserDes();
  }


  getUser () {
    var url = environment.urlBsckend+"user";    
     this.http.get(url).subscribe(datam => {
     this.userInfo=datam;
    });
    
  }
  getUserLog () {
    var url = "http://localhost:8080/auth/user?nombre=yyyy&clave=1234";    
     this.http.get(url).subscribe(datam => {
     
    });
    
  }
  getUserDel () {
    var url = "http://localhost:8080/deleate/user/assd/121234";    
     this.http.get(url).subscribe(datam => {
     
    });
    
  }
  getUserDes () {
    var url = "http://localhost:8080/deslog/user/assd?clav=1349878";    
     this.http.get(url).subscribe(datam => {
     
    });
    
  }

}
