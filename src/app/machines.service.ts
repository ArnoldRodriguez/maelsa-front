import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MachinesService {

  constructor(private http: HttpClient) { }


   getMachines () {
    var url = "http://localhost:8080/machines";    
    return this.http.get(url).subscribe();
   }
    
}
