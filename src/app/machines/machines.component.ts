import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-machines',
  templateUrl: './machines.component.html',
  styleUrls: ['./machines.component.css']
})
export class MachinesComponent implements OnInit {

  isRender: boolean = true;
  title;
  description: string = "Maquina que blblblbalbababalbala........bbalbalab bllba.......lbalablabalbalab bkabkaaka";
 
  machines: any = []
  constructor(private http: HttpClient) { }

  ngOnInit(): void {

    this.getMachines();
    this.getTitle();
  }



  getMachines () {
    var url = "http://localhost:8080/machines";
    
     this.http.get(url).subscribe(datam => {
     this.machines = datam;
    });
    
  }

  getTitle () {
    var url = "http://localhost:8080/title";
    
     this.http.get(url).subscribe(datam => {
       console.log(datam)
     this.title = datam;
    });
    
  }

  

}
