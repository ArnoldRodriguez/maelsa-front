import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Component({
  selector: 'app-userinfo1',
  templateUrl: './userinfo1.component.html',
  styleUrls: ['./userinfo1.component.css']
})
export class Userinfo1Component implements OnInit {

  userInfo
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getUserInfo1();
  }

  getUserInfo1 () {
    var url = environment.urlBsckend+"user1";    
     this.http.get(url).subscribe(datam => {
      this.userInfo = datam;
      console.log(datam)
    });
    
  }

}
